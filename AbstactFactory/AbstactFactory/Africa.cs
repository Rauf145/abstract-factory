﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstactFactory
{
    class Africa : IContinent
    {
        public Carnivore createCarnivore()
        {
            return new Lion();
        }

        public Herbivore createHerbivore()
        {
            return new Wildebeest();
        }
    }
}

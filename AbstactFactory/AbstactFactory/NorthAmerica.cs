﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstactFactory
{
    class NorthAmerica : IContinent
    {
        public Carnivore createCarnivore()
        {
            return new Wolf();
        }

        public Herbivore createHerbivore()
        {
            return new Bison();
        }
    }
}
